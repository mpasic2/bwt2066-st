let broj = 20;
let istina = true;
let rijec = 'Proba';
let niz = ['Sarajevo', 'Tuzla', 'Mostar', 'Zenica'];
let objekat = {
    knjige: ['Pro Git', 'From Mathematics to Generic Programming', 'Managing Data Using Excel', 'The Elements of Style'],
    cijene: [11, 33, 42, 45]
};

let expect = require('chai').expect;
describe('Zadatak 1', () => {
    it('postoji varijabla broj', () => {
      expect(broj).to.be.a('number');
    });

    it('provjera vrijednosti varijable broj' , () =>{
        expect(broj).to.eql(20);
    });

    it('provjera vrijednosti varijable istina' , () =>{
        expect(istina).to.eql(true);
    });

    it('provjera tipa varijable rijec' , () =>{
        expect(rijec).to.be.a('string');
    });

    it('provjera vrijednosti varijable rijec' , () =>{
        expect(rijec).to.have.lengthOf(5);
    });

    it('provjera varijable niz' , () =>{
        expect(niz).to.be.an('array').that.is.not.empty;
    });

    it('provjera tipa varijable rijec' , () =>{
        expect(niz).to.include('Sarajevo','Mostar');
    });

    it('provjera tipa varijable rijec' , () =>{
        expect(objekat).to.have.property('knjige').with.length(4);
    });

    it('provjera tipa varijable rijec' , () =>{
        expect(objekat).to.have.property('cijene').to.include.ordered.members([11, 33, 42, 45]);
    });
});